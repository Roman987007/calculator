package ru.roman.calculator;

import java.lang.String;
import java.util.Scanner;

/**
 * Класс {@code Calculator} содержит методы сложения, вычитания, умножения, деления,
 * вычисления остатка от деления, возведения в степень и вычисления квадратного корня
 *
 * @author Petrushov Roman 14OIT18K
 */
public class Calculator {

    /**
     * Метод сложения двух целых или вещественных чисел
     *
     * @param number1 - слагаемое
     * @param number2 - слагаемое
     * @return - {@code number1 + number2}
     */
    public static double operationAddition(double number1, double number2) {
        return number1 + number2;
    }

    /**
     * Метод вычитания двух целых или вещественных чисел
     *
     * @param number1 - уменьшаемое
     * @param number2 - вычитаемое
     * @return - {@code number1 - number2}
     */
    public static double operationSubtraction(double number1, double number2) {
        return number1 - number2;
    }

    /**
     * Метод умножения двух целых или вещественных чисел
     *
     * @param number1 - множитель
     * @param number2 - множитель
     * @return - {@code number1 * number2}
     */
    public static double operationMultiplication(double number1, double number2) {
        return number1 * number2;

    }

    /**
     * Метод деления двух вещественных чисел
     *
     * @param number1 - делимое
     * @param number2 - делитель
     * @return - {@code number1 / number2}
     */
    public static double operationDivisison(double number1, double number2) {

        if (number2 == 0) {
            throw new ArithmeticException("Деление на ноль невозможно");
        }
        return number1 / number2;
    }

    /**
     * Метод деления двух целых чисел
     *
     * @param number1 - делимое
     * @param number2 - делитель
     * @return - {@code number1 / number2}
     */
    public static int operationDivisison(int number1, int number2) {
        if (number2 == 0) {
            throw new ArithmeticException("Деление на ноль невозможно");
        }
        return number1 / number2;
    }

    /**
     * Метод нахождения остатка от деления целых или вещественных чисел
     *
     * @param number1 - делимое
     * @param number2 - делитель
     * @return - {@code number1 % number2}
     */
    public static int operationResidue(int number1, int number2) {
        if (number2 == 0) {
            throw new ArithmeticException("Деление на ноль невозможно");
        }
        return (number1 % number2);
    }

    /**
     * Метод возведения в степень вещественных чисел
     *
     * @param number1 - основание
     * @param number2 - степень
     * @return - {@code number1}<sup>{@code number2}</sup>
     */
    public static double operationExponentiation(double number1, int number2) {
        double exp = 1;
        if (number2 == 0){
            return 1;
        }
        else if (number2 == 1){
            return number1;
        }
        else if (number2 == -1){
            exp *= 1/ number1;
        }
            for (int i = 1; i <= number2; i++) {
                exp *= number1;
            }
        return exp;
    }

    /**
     * Метод извлечения квадратного корня
     *
     * @param number1 - первый операнд
     * @return - {@code √number1}
     */
    public static double operationRootExtract(double number1) {
        if (number1 == 0) {
            return 0;
        }

        if (number1 < 0) {
            throw new ArithmeticException("Подкоренное выражение не может быть отрицательным!");
        }
        double x = 1;
        double EPS = 1E-15;
        double nx = (x + number1 / x) / 2;
        while (Math.abs(x - nx) > EPS) {
            x = nx;
            nx = (x + number1 / x) / 2;
        }
        return x;
    }

}


