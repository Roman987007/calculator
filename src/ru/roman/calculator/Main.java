package ru.roman.calculator;

import java.io.*;
import java.lang.String;
import java.util.Scanner;

import static java.lang.Double.parseDouble;


/**
 * Класс предназначен для ввода и вывода данных и выполнения арифметических действий.
 */
public class Main {

    public static void main(String[] args) throws IOException {


        InputAndOutputDataInFile("inputFile.txt", "outFile.txt");

        String str = inputStr();
        String[] data;
        double result;
        data = str.split(" ");
        double number1 = Double.parseDouble(data[0]);
        double number2 = Double.parseDouble(data[1]);
        result = OperatorSelection(number1, number2, data[2]);
        outputStr(number1, number2, result, data[2]);

    }


    /**
     * Метод для ввода данных через консоль
     *
     * @return - возвращает введенную строку
     */
    public static String inputStr() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите строку вида число число знак: ");
        String str = scan.nextLine();
        scan.close();
        return str;
    }

    /**
     * Медот для вывода данных в консоль
     *
     * @param number1   - первый операнд
     * @param operation - знак операции
     * @param number2   - второй операнд
     * @param result    - результат вычисления
     */
    public static void outputStr(double number1, double number2, double result, String operation) {
        System.out.print(number1 + " " + operation + " " + number2 + " = " + result);
    }

    public static void InputAndOutputDataInFile(String inputFile, String outputFile) throws IOException {
        String[] data;
        String str;

        double result;

        BufferedReader InputFile = new BufferedReader(new FileReader(inputFile));
        BufferedWriter OutputFile = new BufferedWriter(new FileWriter(outputFile));

        while ((str = InputFile.readLine()) != null) {
            if (!CheckStr(str)) {
                continue;
            }
            data = str.split(" ");
            double number1 = Double.parseDouble(data[0]);
            double number2 = Double.parseDouble(data[1]);
            result = OperatorSelection(number1, number2, data[2]);
            OutputFile.write(number1 + " " + data[2] + " " + number2 + " = " + result + "\r\n");
        }

        InputFile.close();
        OutputFile.close();

    }

    public static boolean CheckStr(String str) {
        String[] inputData = str.split(" ");

        if (str.length() == 0) {
            throw new NullPointerException("Пустая строка!");
        }

        if (inputData.length != 3) {
            System.out.println("Не введён операнд или знак операции!");

        try {
            Double.parseDouble(inputData[0]);
        } catch (NumberFormatException e) {
            System.out.println("Неверное значение 1 операнда! " + e);
            return false;
        }
        try {
            Double.parseDouble(inputData[1]);
        } catch (NumberFormatException e) {
            System.out.println("Неверное значение 2 операнда! " + e);
            return false;
        }

        if (inputData[2].length() != 1) {
            System.out.println("Знак операции состоит только из 1 символа!");
        }
        char[] sign = {'+', '-', '*', '/', '%', '^', '#'};
        boolean flag = true;

        for (int i = 0; i < sign.length; i++) {
            if (sign[i] == (inputData[2]).charAt(0)) {
                flag = false;
                break;
            }
        }
        if (flag) {
            System.out.println("Неверный знак операции!");
            return false;
        }
        return false;
    }
    return true;
}

    /**
     * Метод для выбора операцит вычисления из класса {@code Calculator}по передаваемому знаку операции,
     * возвращает результат выбранной операции
     *
     * @param number1 - первый операнд
     * @param number2 - второй операнд
     * @param operation - знак операции
     * @return - результат выбранного метода вычисления
     */
    public static double OperatorSelection(double number1, double number2, String operation){

        int numberInt1 = (int) number1;
        int numberInt2 = (int) number2;

        double result = 0;
        switch (operation) {
            case "+":
                result = Calculator.operationAddition(number1, number2);
                break;
            case "-":
                result = Calculator.operationSubtraction(number1, number2);
                break;
            case "*":
                result = Calculator.operationMultiplication(number1, number2);
                break;
            case "/":
                if (number1 % 1 == 0 && number2 % 1 == 0) {
                    result = Calculator.operationDivisison(numberInt1, numberInt2);
                } else {
                    result = Calculator.operationDivisison(number1, number2);
                }
                break;
            case "%":
                if (number1 % 1 == 0 && number2 % 1 == 0) {
                    result = Calculator.operationResidue(numberInt1, numberInt2);
                } else {
                    System.out.println("Неверные значения операндов при выделении остатка!");

                }
                break;
            case "^":
                result = Calculator.operationExponentiation(number1, (int)number2);
                break;
            case "#":
                result = Calculator.operationRootExtract(number1);
                break;
        }
        return result;
    }
}
